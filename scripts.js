$(document).ready(function(){
    $('.mobile-menu-icon').click(function(){
        $(this).toggleClass('open');
        if($(".mobile-menu-icon").hasClass('open')){
            $("nav ul").fadeIn();
        } else {
            $("nav ul").fadeOut();
        }
    });

    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.hero-slider').slick({
        nextArrow: '.custom_next',
        prevArrow: '.custom_prev'
    });

    $('.slider.testimon').slick({
        nextArrow: '.testimon_next',
        prevArrow: '.testimon_prev'
    });
});